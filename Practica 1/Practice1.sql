/*First exercise*/ 
CREATE DATABASE Tienda;

/*Second exercise*/
DROP DATABASE Tienda;

/*Third exercise*/
USE Tienda
CREATE TABLE Clientes (
Id int PRIMARY KEY,
Nombre varchar(50) NULL,
Apellido varchar(50) NULL,
Edad int NULL)

/*Fourth exercise*/
CREATE TABLE Productos (
CodProducto int PRIMARY KEY,
Nombre varchar(50) NULL,
Precio int NULL)

/*Fifth exercise*/
CREATE TABLE Ventas (
Id int PRIMARY KEY,
IdProducto int NOT NULL,
IdCliente int NOT NULL,
Fecha date NULL)

/*Sixth exercise*/
USE Tienda
DROP TABLE Cliente;
DROP TABLE Productos;
DROP TABLE Ventas;

/*Seventh exercise*/
USE Tienda
GO
EXEC sp_rename 'Clientes.Apellido', 'Apellidos', 'COLUMN';
GO

/*Eighth exercise*/
USE Tienda
TRUNCATE TABLE Ventas

/*Nineth exercise*/
Use Tienda
INSERT INTO Clientes (Id, Nombre, Apellido, Edad) 
VALUES (‘5131651’, ‘Jaime’, ‘Rocha’, ‘32’)
(‘1651635’, ‘Juan’, ‘Marquez’, ‘25’)
(‘516516, ‘Maria’, ‘Nogales’, ‘18)
(‘5158416’, ‘Pedro’, ‘Gonzalez’, ‘26’)
(‘5163341’, ‘Julia’, ‘Vasquez’, ‘30’)

INSERT INTO Productos (CodProducto, Nombre, Precio) 
VALUES (‘65461’, ‘Leche’, ‘10’)
(‘51631’, ‘Gelatina’, ‘3’)
(‘65161’, ‘Carne Parrillera’, ‘35’)
(‘51315’, ‘Manzanas’, ‘5’)
(‘5158416’, ‘Arroz’, ‘12’)

INSERT INTO Ventas (Id, IdProducto, IdCliente, Fecha) 
VALUES (‘3165165’, ‘1651635’, ‘65461’, ’05-09-2018’)
(‘36151635’, ‘51315’, ‘5158416’, ’09-10-2018’)
(‘51651131’, ‘5158416’, ‘516516’, ’20-10-2018’)
(‘15151635’, ‘5158416’, ‘5163341’, ’12-12-2018’)
(‘321651654’, ‘5158416’, ‘5158416’, ’20-11-2018’)

/*Tenth exercise*/
Select *
From Ventas

/*Eleventh exercise*/
Select TOP 10
From Clientes

/*Twelfth exercise*/
Select *
From Clientes
Group By Edad

/*Thirteenth exercise*/
Select *
From Clientes
Where Edad > 20

/*Fourteenth exercise*/
Delete From Clientes
Where Edad >= 20