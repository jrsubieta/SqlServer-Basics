--Using Full Outer Join
USE Libreria
SELECT Persona.nombre, Libro.precio
FROM Persona
FULL OUTER JOIN Libro ON Persona.persona_id = Libro.persona_id;


SELECT Persona.*, Libro.*
FROM Persona
FULL OUTER JOIN Libro ON Persona.persona_id = Libro.persona_id;