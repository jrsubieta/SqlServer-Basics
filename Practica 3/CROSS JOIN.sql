/*Crear las tablas Personas y Libro*/




CREATE TABLE Personas(persona_id SMALLINT PRIMARY KEY  NULL, Nombre varchar(50)NOT NULL,Apellido varchar(50)NULL,Genero CHAR(1) NOT NULL, prestamoLibro VARCHAR NULL (10),Pais VARCHAR(50) NULL)


CREATE TABLE Libro(
	libro_id SMALLINT NOT NULL,
	titulo VARCHAR(50) NOT NULL,
	precio DECIMAL(5,2) NULL,
	persona_id SMALLINT  NULL,
	prestamoLibro VARCHAR (10),
	PRIMARY KEY (libro_id),
	FOREIGN KEY(persona_id) REFERENCES Personas (persona_id));


	SELECT * FROM Personas;
	SELECT * FROM Libro;


	/* llenar datos a las Tablas*/
	INSERT INTO Personas 
 VALUES(1,'Maria','Rodriguez', 'F', 'SI','Bolivia'),
       (2,'Pedro','Marquez', 'M', 'SI','Ecuador'),
       (3,'Ali','Perez', 'M', 'NO','Colombia'),
       (4,'Juan','Loayza', 'M', 'SI','Argentina'),
       (5,'Fernanda','Mendez', 'F', 'NO','Peru'),
       (6,'Lucero','Alba', 'F', 'SI','Chile'),
       (7,'Luis','Smith', 'M', 'SI','EEUU'),
	   (8,'Antonia','Sanchez', 'F', 'NO','Singapur'),
	   (9,'Francisco','Montas', 'M', 'NO','Bolivia'),
	   (10,'Claudia','Villarroel', 'F', 'SI','Bolivia'),
	   (11,'Juan Jose','Garcia', 'M', 'SI','Bolivia'),
       (12,'Jaime','Rocha', 'M', 'SI','Bolivia'),
       (13,'Aurelio','Valdez', 'M', 'NO','Italia'),
       (14,'Ana','Sousa', 'M', 'SI','Brasil'),
       (15,'Julio','Quiroga', 'M', 'SI','Peru')
       


	   INSERT INTO Libro
 VALUES(50,'Las mil y una noches','50', 1,'SI'),
       (51,'La Odisea','150',10,'SI'),
       (52,'La Iliada','160',2,'SI'),
       (53,'Aladino','30',11,'SI'),
       (54,'La Biblia','80',3,'SI'),
       (55,'El Coran','80', 12,'SI'),
       (56,'Metafisica','90', 10,'SI'),
       (57,'Arpas Eternas','200', 4,'SI'),
       (58,'Origenes Adanicos','350', 5,'SI'),
       (59,'Sabes lo que crees?','160',6,'SI'),
	   (60,'Mi planta de naranjaLima','120',11,'SI'),
	   (61,'Historia de Bolivia','400',12,'SI'),
	   (62,'Juan Salvador Gaviota','45',10,'SI'),
	   (63,'Cien anios de Soledad','270',11,'SI'),
	   (64,'Arpas Eternas II','200',12,'SI'),
	   (65,'Arpas Eternas II',NULL,NULL,NULL)
	   

	   SELECT * FROM Personas CROSS JOIN Libro;
	 



