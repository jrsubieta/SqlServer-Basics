USE Libreria

--Using Left join 
SELECT Persona.nombre, Persona.apellido, Libro.titulo, Libro.precio
FROM Persona
LEFT JOIN Libro
ON Persona.persona_id = Libro.persona_id


SELECT Persona.* ,Libro.*
FROM Persona
LEFT JOIN Libro
ON Persona.persona_id = Libro.persona_id
