Use Libreria

Select *
From Libro

Select *
From Persona

/* Lista de personas por nombre, apellido, genero que no tengan ningun Libro asociado por izquierda y ademas no tengan nacionalidad*/ 
Select P.nombre, P.apellido, P.genero, L.titulo
From Persona as P
LEFT JOIN Libro as L  on P.persona_id = L.persona_id
Where L.libro_id is null and P.pais is null
Order By P.nombre

/* Lista de personas agrupadas por genero que no tengan ningun libro asociado por izquierda*/
Select Count(P.persona_id) as 'Numero de Personas', P.genero
From Persona as P
LEFT JOIN Libro as L  on P.persona_id = L.persona_id
Where L.libro_id is null
Group By P.genero