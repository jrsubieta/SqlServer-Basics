/* 1. Registrar 7 personas con datos completos */

INSERT INTO Persona (Persona_id, nombre, apellido, genero, fechaNacimiento, pais)
VALUES (1, 'Vanesa', 'Espinoza', 'F', '1985-10-08', 'Bolivia'),	
	   (2, 'Marco', 'Rodriguez', 'M', '1985-11-09', 'Bolivia'),
	   (3, 'Carlos', 'Torrez', 'M', '1980-08-02', 'Bolivia'),
	   (4, 'Lilian', 'Rocha', 'F', '1986-12-23', 'Bolivia'),
	   (5, 'Lorena', 'Leitao', 'F', '1990-01-08', 'Brasil'),
	   (6, 'Julia', 'Gomez', 'F', '1991-05-20', 'Colombia'),
	   (7, 'Patricia', 'Correa', 'F', '1985-01-30', 'Mexico')
	   
 /* 2. Registrar 3 personas sin nacionalidad */|

 INSERT INTO Persona (Persona_id, nombre, apellido, genero, fechaNacimiento, pais)
 VALUES (8, 'Joan', 'Fernandez', 'M', '1992-10-20', NULL),
		(9, 'Felipe', 'Gandariña', 'M', '1995-08-10', NULL),
		(10, 'Roxana', 'Cabrera', 'F', '1981-03-10', NULL)
		
 /* 3. Registrar 1 persona sin nacionalidad ni fecha de nacimiento */

 INSERT INTO Persona (Persona_id, nombre, apellido, genero, fechaNacimiento, pais)
 VALUES (11, 'Boris', 'Flores', 'M', NULL, NULL)
 
 /* 4. Registrar 10 libros */

 INSERT INTO Libro (libro_id, titulo, precio, persona_id)
 VALUES (10, 'Los Juegos del Hambre', '52',6),
		(20, 'Amor y guerra', '52',5),
		(30, 'Ciudad de Hueso', '85',2),
		(40, 'Calculo II', '65',5),
		(50, 'Harry Potter', '150',2),
		(60, 'SQL Server', '5',6),
		(70, 'Elena', '98',11),
		(80, 'Calculo I', '45',4),
		(90, 'Realidad Dispersa', '38',1),
		(100, 'Juego de tronos', '63',10)
		
/* 5. Listar el nombre y apellido de la tabla “Persona” ordenados por apellido y luego por nombre. **** */

SELECT apellido, nombre
FROM Persona
ORDER BY apellido, nombre

/* 6. Listar los libros con precio mayor a 50 Bs. y que el titulo del libro empieze con la letra “A” */

SELECT *
FROM Libro
WHERE precio > 50 AND LEFT(titulo, 1) = 'A'

/* 7. Listar el nombre y apellido de las personas que tengan al menos un libro cuyo precio sea mayor a 50 Bs. */

SELECT P.nombre, P.apellido, L.titulo
FROM Persona as P, Libro as L
WHERE P.Persona_id = L.persona_id AND L.precio > 50

/* 8. Listar el nombre y apellido de las personas que nacieron el año 1990 */

SELECT nombre, apellido
FROM Persona
WHERE fechaNacimiento LIKE '1990%'

/* 9. Listar los libros donde el titulo del libro contenga la letra “e” en la segunda posición */

SELECT *
FROM Libro
WHERE titulo LIKE '_e%'

/* 10.Contar el numero de registros de la tabla “Persona" */

SELECT COUNT(*)
FROM Persona

/* 11. Listar el o los libros más barato */

SELECT MIN(precio)
FROM Libro

/* 12. Listar el o los libros más caros */

SELECT MAX(precio)
FROM Libro

/* 13. Mostrar el promedio de precios de los libro */

SELECT AVG(precio)
FROM Libro

/* 14. Mostrar la sumatoria de los precios de los libros */

SELECT SUM(precio)
FROM Libro

/* 15. Modificar la longitud del campo titulo para que soporte hasta titulos de 300 caracteres */

ALTER TABLE Libro
ALTER COLUMN titulo varchar(300) null;

/* 16. Listar las personas que sean del país de Colombia ó de Mexico */

SELECT *
FROM Persona
WHERE pais = 'Colombia' OR pais = 'Mexico'

/* 17. Listar los libros que no empiezen con la letra “T” */

SELECT *
FROM Libro
WHERE NOT LEFT(titulo, 1) = 'T'

/* 18. Listar los libros que tengan un precio entre 50 a 70 Bs. */

SELECT *
FROM Libro
WHERE precio BETWEEN 50 and 70

/* 19. Listar las personas AGRUPADOS por país */

SELECT COUNT(*), pais
FROM Persona
GROUP BY pais

/* 20. Listar las personas AGRUPADOS por país y genero */

SELECT COUNT(*), pais, genero
FROM Persona
GROUP BY pais, genero
ORDER BY pais

/* 21. Listar las personas AGRUPADOS por genero, cuya cantidad sea mayor a 5 */

SELECT *
FROM Persona
GROUP BY genero
HAVING COUNT(Persona_id) > 5

/* 22. Listar los libros ordenados por titulo en orden descendente */

SELECT *
FROM Libro
ORDER BY titulo DESC

/* 23. Eliminar el libro con el titulo “SQL Server” */

DELETE FROM Libro
WHERE titulo = 'SQL Server'

 /* 24. Eliminar los libros que comiencen con la letra “A” */
 
 DELETE FROM Libro
 WHERE LEFT(titulo, 1) = 'A'
 
 /* 25. Eliminar las personas que no tengan libros */
 
 DELETE FROM Persona
 WHERE Persona_id  NOT IN (SELECT DISTINCT(persona_id) FROM Libro)
 
 /* 26 Eliminar todos los libros */
 
 TRUNCATE TABLE Libro
 
 /* 27. Modificar el nombre de la persona llamada “Marco” por “Marcos" */
 
 UPDATE Persona SET Nombre = 'Marcos'
 WHERE Nombre = 'Marco'
 
 /* 28 Modificar el precio del todos los libros que empiezen con la palabra “Calculo”  a  57 Bs. */
 
 UPDATE Libro SET precio = 57
 WHERE titulo LIKE 'Calculo%'
 
 /* 29. Inventarse una consulta con GROUP BY  y HAVING  y explicar el resultado que retorna. */
 
 /* Lista de todas las personas agrupadas por genero cuya cantidad es mayor a 2 */
 
 SELECT COUNT(*), genero 
 FROM Persona
 GROUP BY genero
 HAVING COUNT(persona_id) > 2
 
