
	/* Integrantes: Claudia Villarroel / Juan Jose Garcia / Jaime Rocha */

	/*VIEW*/

	 /* EXAMPLE 1 : VIEW*/


	  CREATE VIEW  [VISTA PERSONA]
	  AS
	  SELECT P.Nombre,P.Apellido,P.Genero,P.Pais,L.titulo,L.prestamoLibro
	  FROM Personas P INNER JOIN Libro L ON P.persona_id= L.persona_id;



	   /* EXAMPLE 2 : VIEW*/

      CREATE VIEW [Books that no bolivian people have] AS
      SELECT P.nombre, P.apellido, P.pais, L.titulo
      FROM Persona P
      INNER JOIN Libro L ON P.persona_id = L.persona_id
      WHERE pais != 'Bolivia'

      SELECT * FROM [Books that no bolivian people have]


	  /* EXAMPLE 3 : VIEW*/

      CREATE VIEW [Americans] AS
      SELECT nombre, apellido, pais
      FROM Persona
      WHERE pais = 'EEUU'

      SELECT * FROM [Americans]




	   /*STORE PROCEDURE*/

	   /* EXAMPLE 1: PROCEDURE */

	  CREATE PROCEDURE PRESTAMO
	  AS
	  SELECT P.Nombre,P.Apellido,P.Genero,P.Pais,L.titulo, L.prestamoLibro
	  FROM Personas AS P RIGHT outer JOIN Libro AS L ON P.prestamoLibro= L.prestamoLibro
	   WHERE L.persona_id IS NULL


	   EXECUTE PRESTAMO
	   GO



	    /* EXAMPLE 2: PROCEDURE */


		CREATE PROCEDURE TODOS
		AS
		SELECT Personas.Nombre,Personas.Apellido,Personas.Genero,Personas.Pais,Libro.titulo, Libro.prestamoLibro
	    FROM Personas FULL OUTER JOIN Libro ON Personas.prestamoLibro= Libro.prestamoLibro


	   EXECUTE TODOS
		GO



		 /* EXAMPLE 3: PROCEDURE */

       CREATE PROCEDURE [Update price of books]
       AS
       UPDATE Libro
       SET precio = 50

       EXEC [Update price of books]

       

	   /* EXAMPLE 4: PROCEDURE */

       CREATE PROCEDURE [List of Brazilian females.]
       AS
       SELECT nombre, apellido, pais
       FROM Persona
       WHERE pais='Brasil' AND genero='f'

       EXEC[List of Brazilian females.]


      /* EXAMPLE 5: PROCEDURE */

      CREATE PROCEDURE [List of americans who were born in november.]
      AS
      SELECT nombre, apellido, fechaNacimiento
      FROM Persona
      WHERE MONTH(fechaNacimiento)=11 AND pais='EEUU'

      EXEC [List of americans who were born in november.]