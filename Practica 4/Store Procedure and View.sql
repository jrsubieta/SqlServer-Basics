--Stored procedured 1
CREATE PROCEDURE [Update price of books]
AS
UPDATE Libro
SET precio = 50

EXEC [Update price of books]

select * from Libro

-- View 1
CREATE VIEW [Books that no bolivian people have] AS
SELECT P.nombre, P.apellido, P.pais, L.titulo
FROM Persona P
INNER JOIN Libro L ON P.persona_id = L.persona_id
WHERE pais != 'Bolivia'

SELECT * FROM [Books that no bolivian people have]


